﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static ProjectWFA.View.Utils.GridViewHelper;

namespace ProjectWFA
{
    public partial class FormViewSubject : Form
    {
        public FormViewSubject()
        {
            InitializeComponent();
            InitializeGrid(subjectGrid, c => c.LoadSubjects(), delegate { });
        }
    }
}
