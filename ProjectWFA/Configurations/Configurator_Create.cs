﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA.Configurations
{
    partial class Configurator
    {
        public void SaveSpecialty(int id, string name)
        {
            var parameters = new Dictionary<string, Action<SqlCommand>>
            {
                { "SpecialtyId", cmd => AddParameter(cmd, "SpecialtyId", SqlDbType.Int, id)},
                { "Name", comm => AddParameter(comm, "Name", SqlDbType.VarChar, name)}
            };

            SaveEntity("Specialty", parameters);
        }

        public void SaveSubject(int id, string name)
        {
            var parameters = new Dictionary<string, Action<SqlCommand>>
            {
                {"SubjectId", cmd => AddParameter(cmd, "SubjectId", SqlDbType.Int, id) },
                { "Name", cmd => AddParameter(cmd, "Name", SqlDbType.VarChar, name)}
            };

            SaveEntity("Subject", parameters);
        }

        public void SaveStudent(int fNumber, int specialtyId, string fName, string mName, string lName, string address, string phone, string eMail)
        {
            var parameters = new Dictionary<string, Action<SqlCommand>>
            {
                { "FNumber", cmd => AddParameter(cmd, "FNumber", SqlDbType.Int, fNumber)},
                {"SpecialtyId", cmd => AddParameter(cmd, "SpecialtyId", SqlDbType.Int, specialtyId) },
                {"FName", cmd => AddParameter(cmd, "FName", SqlDbType.VarChar, fName) },
                {"MName", cmd => AddParameter(cmd, "MName", SqlDbType.VarChar, mName) },
                {"LName", cmd => AddParameter(cmd, "LName", SqlDbType.VarChar, lName) },
                {"Address", cmd => AddParameter(cmd, "Address", SqlDbType.VarChar, address) },
                {"Phone", cmd => AddParameter(cmd, "Phone", SqlDbType.VarChar, phone) },
                {"EMail", cmd => AddParameter(cmd, "EMail", SqlDbType.VarChar, eMail) }
            };

            SaveEntity("Student", parameters);

        }

        public void SaveGrade(int fNumber, int subjectId, int finalGrade)
        {
            var parameters = new Dictionary<string, Action<SqlCommand>>
            {
                { "FNumber", cmd => AddParameter(cmd, "FNumber", SqlDbType.VarChar, fNumber)},
                { "SpecialtyId", cmd => AddParameter(cmd, "SpecialtyId", SqlDbType.Int, subjectId)},
                { "FinalGrade", cmd => AddParameter(cmd, "FinalGrade", SqlDbType.Int, finalGrade)}

            };

            SaveEntity("Grade", parameters);
        }

        private void SaveEntity(string tableName, Dictionary<string, Action<SqlCommand>> parameters)
        {
            using (var connection = manipulator.Connection)
            {
                try
                {
                    connection.Open();
                    var command = manipulator.Command;
                    var columns = string.Join(", ", parameters.Keys);
                    var parameterPlaceholders = string.Join(", ", parameters.Keys.Select(p => $"@{p}"));

                    command.CommandText = $"INSERT INTO {tableName} ({columns}) VALUES ({parameterPlaceholders})";

                    foreach (var parameter in parameters.Values)
                    {
                        parameter(command);
                    }

                    command.ExecuteNonQuery();
                }
                catch (Exception e)
                {
                    MessageBox.Show(e.ToString());
                }
            }
        }

        private void AddParameter(SqlCommand command, string variableName, SqlDbType type, object value)
        {
            var parameter = new SqlParameter($"@{variableName}", type);
            parameter.Value = value;
            command.Parameters.Add(parameter);
        }

    }
}
