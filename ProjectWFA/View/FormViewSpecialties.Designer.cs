﻿namespace ProjectWFA
{
    partial class FormViewSpecialties
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.specialtiesGrid = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.specialtiesGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // specialtiesGrid
            // 
            this.specialtiesGrid.AllowUserToAddRows = false;
            this.specialtiesGrid.AllowUserToDeleteRows = false;
            this.specialtiesGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.specialtiesGrid.Location = new System.Drawing.Point(12, 12);
            this.specialtiesGrid.Name = "specialtiesGrid";
            this.specialtiesGrid.ReadOnly = true;
            this.specialtiesGrid.Size = new System.Drawing.Size(776, 426);
            this.specialtiesGrid.TabIndex = 0;
            // 
            // FormViewSpecialties
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.specialtiesGrid);
            this.Name = "FormViewSpecialties";
            this.Text = "FormViewSpecialties";
            this.Load += new System.EventHandler(this.FormViewSpecialties_Load);
            ((System.ComponentModel.ISupportInitialize)(this.specialtiesGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView specialtiesGrid;
    }
}