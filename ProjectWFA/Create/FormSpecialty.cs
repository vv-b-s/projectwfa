﻿using ProjectWFA.Configurations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA
{
    public partial class FormSpecialty : Form
    {
        public FormSpecialty()
        {
            InitializeComponent();
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            int specialtyID = (int)this.specialtyID.Value;
            var specialtyName = this.specialtyName.Text;

            var configurator = Configurator.Instance;
            configurator.SaveSpecialty(specialtyID, specialtyName);
        }
    }
}
