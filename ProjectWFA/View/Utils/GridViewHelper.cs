﻿using ProjectWFA.Configurations;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA.View.Utils
{
    class GridViewHelper
    {

        public static void InitializeGrid(DataGridView grid, Func<Configurator, DataTable> loader, Action<object, DataGridViewCellEventArgs> onRowClick )
        {
            var configurator = Configurator.Instance;
            var table = loader(configurator);

            grid.DataSource = table;
            grid.CellDoubleClick += onRowClick.Invoke;
            grid.Refresh();
        }


        public static DataGridViewRow GetRowFromSelectedCell(DataGridView dataGrid)
        {
            var cells = dataGrid.SelectedCells;
            return cells.Count == 1 ? dataGrid.Rows[cells[0].RowIndex] : null;
        }
    }
}
