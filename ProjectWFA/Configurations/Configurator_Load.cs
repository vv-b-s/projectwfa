﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA.Configurations
{
    partial class Configurator
    {
        public DataTable LoadSpecialties()
        {
            var columns = new Dictionary<string, Func<object, object>>
            {
                {"SpecialtyId", sId => Convert.ToInt32(sId) },
                { "Name", n => Convert.ToString(n)}
            };

            return BuildTable("Specialty", columns, "Name");

        }

        public DataTable LoadStudents()
        {
            var columns = new Dictionary<string, Func<object, object>>
            {

                { "FNumber", fn => Convert.ToString(fn)},
                { "SpecialtyId", spi => Convert.ToInt32(spi)},
                { "FName", fname => Convert.ToString(fname)},
                { "LName", lname => Convert.ToString(lname)},
                { "Address", a => Convert.ToString(a)},
                {"Phone", p => Convert.ToString(p) },
                { "EMail", e => Convert.ToString(e)}
            };

            return BuildTable("Student", columns, "FNumber");
        }

        public DataTable LoadSubjects()
        {
            var columns = new Dictionary<string, Func<object, object>>
            {
                { "SubjectId", sjid => Convert.ToInt32(sjid)},
                { "Name", sn => Convert.ToString(sn)}
            };

            return BuildTable("Subject", columns, "Name");
        }

        public DataTable LoadGrades()
        {
            var columns = new Dictionary<string, Func<object, object>>
            {
                { "FNumber", fn => Convert.ToInt32(fn) },
                {"SpecialtyId", spid => Convert.ToInt32(spid)},
                { "FinalGrade", fingr => Convert.ToInt32(fingr) }
            };

            return BuildTable("Grade", columns);
        }

        private DataTable BuildTable(string tableName, Dictionary<string, Func<object, object>> columns, string orderBy = "")
        {
            var result = new DataTable();

            foreach (var key in columns.Keys)
            {
                result.Columns.Add(key);
            }

            using (var connection = manipulator.Connection)
            {
                try
                {
                    connection.Open();
                    var command = manipulator.Command;
                    var queryItems = string.Join(", ", columns.Keys);
                    var orderClause = string.IsNullOrEmpty(orderBy) || string.IsNullOrWhiteSpace(orderBy) ? "" : $"ORDER BY {orderBy} ASC";

                    command.CommandText = $"SELECT {queryItems} FROM {tableName} {orderClause}";
                    var reader = command.ExecuteReader();

                    using (reader)
                    {
                        while (reader.Read())
                        {
                            var row = result.NewRow();

                            int index = 0;
                            foreach (var column in columns)
                            {
                                row[index] = column.Value(reader[column.Key]);
                                index++;
                            }

                            result.Rows.Add(row);
                        }
                    }

                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    MessageBox.Show(e.Message);
                }
            }

            return result;
        }
    }
}
