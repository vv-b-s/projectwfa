﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA.Configurations
{
    class DBManipulator
    {
        private const string ConnectionString = @"Server=(localdb)\MSSQLLocalDB;Initial Catalog=ISDA_15118042;Integrated Security=True";

        private SqlConnection connection;
        private SqlCommand command;

        public DBManipulator()
        {
            try
            {
                this.connection = new SqlConnection(ConnectionString);
                this.command = new SqlCommand();

                this.command.Connection = connection;

            }

            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        public SqlConnection Connection
        {
            get => connection;
        }

        public SqlCommand Command
        {
            get
            {
                this.command.Parameters.Clear();
                return this.command;
            }
        }
    }
}
