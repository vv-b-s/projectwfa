﻿using ProjectWFA.Configurations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using static ProjectWFA.View.Utils.GridViewHelper;

namespace ProjectWFA
{
    public partial class FormViewSpecialties : Form
    {
        public FormViewSpecialties()
        {
            InitializeComponent();
        }

        private void FormViewSpecialties_Load(object sender, EventArgs e)
        {
            InitializeGrid(specialtiesGrid, c => c.LoadSpecialties(), OpenEditWindow);
        }

        private void OpenEditWindow(object sender, DataGridViewCellEventArgs e)
        {
            var row = GetRowFromSelectedCell((DataGridView)sender);
            if (row != null)
            {
                int id = Convert.ToInt32(row.Cells[0].Value);
                string name = row.Cells[1].Value.ToString();

                var form = new FormEditSpecialty();
                form.Init(id, name);
                form.ShowDialog();
            }
        }
    }
}
