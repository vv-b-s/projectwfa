﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA.Configurations
{
    partial class Configurator
    {
        private DBManipulator manipulator;

        private Configurator()
        {
            this.manipulator = new DBManipulator();
        }

        public static Configurator Instance => new Configurator();
    }
}
