﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void specialtyToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormSpecialty().ShowDialog();
        }

        private void subjectToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormSubject().ShowDialog();
        }

        private void studentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormStudent().ShowDialog();
        }

        private void gradeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new FormGrade().ShowDialog();
        }

        private void specialtyToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FormViewSpecialties().ShowDialog();
        }

        private void studentToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FormViewStudent().ShowDialog();
        }

        private void subjectToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FormViewSubject().ShowDialog();
        }

        private void gradeToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            new FormViewGrade().ShowDialog();
        }
    }
}
