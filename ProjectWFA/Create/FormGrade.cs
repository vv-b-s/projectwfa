﻿using ProjectWFA.Configurations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA
{
    public partial class FormGrade : Form
    {
        public FormGrade()
        {
            InitializeComponent();
        }

        private void FormGrade_Load(object sender, EventArgs e)
        { 
            DataTable dTableStudents = Configurator.Instance.LoadStudents();

            studentCombo.DataSource = dTableStudents;
            studentCombo.ValueMember = "FNumber";
            studentCombo.DisplayMember= "FName";

            DataTable dTableSubjects = Configurator.Instance.LoadSubjects();

            subjectCombo.DataSource = dTableSubjects;
            subjectCombo.ValueMember = "SubjectId";
            subjectCombo.DisplayMember = "Name";
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Configurator configurator = Configurator.Instance;
            configurator.SaveGrade(int.Parse(studentCombo.SelectedValue.ToString()), int.Parse(studentCombo.SelectedValue.ToString()), (int)gradeNumeric.Value);
        }
    }
}
