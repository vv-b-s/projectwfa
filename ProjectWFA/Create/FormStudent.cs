﻿using ProjectWFA.Configurations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA
{
    public partial class FormStudent : Form
    {
        public FormStudent()
        {
            InitializeComponent();
        }

        private void FormStudent_Load(object sender, EventArgs e)
        {
            Configurator configurator = Configurator.Instance;

            DataTable dTable = configurator.LoadSpecialties();

            this.specialtyCombo.DataSource = dTable;
            this.specialtyCombo.ValueMember = "SpecialtyId";
            this.specialtyCombo.DisplayMember = "Name";
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            Configurator configurator = Configurator.Instance;
            configurator.SaveStudent((int)this.facultyNumber.Value, int.Parse(this.specialtyCombo.SelectedValue.ToString()), this.firstName.Text, this.middleName.Text, this.lastName.Text, this.address.Text, this.phone.Text, this.email.Text);
        }
    }
}

