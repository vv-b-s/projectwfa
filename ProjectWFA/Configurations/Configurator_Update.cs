﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA.Configurations
{
    partial class Configurator
    {
        public void UpdateSpecialty(int originalId, int id, string name)
        {
            

            using(SqlConnection connection = this.manipulator.Connection)
            {
                try
                {
                    connection.Open();

                    SqlCommand command = this.manipulator.Command;

                    command.CommandText = "UPDATE Specialty SET SpecialtyId = @SpecialtyId, Name = @Name WHERE SpecialtyId = @OriginalId";

                    SqlParameter param = null;

                    param = new SqlParameter("@SpecialtyId", SqlDbType.Int);
                    param.Value = id; command.Parameters.Add(param);

                    param = new SqlParameter("@Name", SqlDbType.VarChar); param.Value = name; command.Parameters.Add(param);

                    param = new SqlParameter("@OriginalId", SqlDbType.Int); param.Value = originalId; command.Parameters.Add(param);

                    command.ExecuteNonQuery();
                }
                catch (Exception e) { MessageBox.Show(e.ToString()); }
            }
            
        }
    }
}
