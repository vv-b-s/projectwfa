﻿using ProjectWFA.Configurations;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProjectWFA
{
    public partial class FormEditSpecialty : Form
    {
        private int id;

        public FormEditSpecialty()
        {
            InitializeComponent();
        }

        public void Init(int id, string name)
        {
            this.id = id;

            specialtyID.Value = id;
            specialtyName.Text = name;
        }

        private void saveBtn_Click(object sender, EventArgs e)
        {
            Configurator configurator = Configurator.Instance;
            configurator.UpdateSpecialty(id, (int)specialtyID.Value, specialtyName.Text);
        }
    }
}
